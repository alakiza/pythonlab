#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class StringFormatter:
    def __init__(self, string):
        self._string = string
    
    def DeleteWordsShortestThat(self, LetterCount):
        self._string = ' '.join(list(filter(lambda x: len(x) >= LetterCount, list(self._string.split()))))
        return self._string
    
    def MaskDecimal(self):
        def MaskDecimal(ch):
            if ch.isdecimal():
                return '*'
            else:
                return ch
                
        self._string = ''.join(list(map(MaskDecimal, self._string)))
        return self._string
    
    def InsertWhiteSpace(self):
        self._string = ' '.join(list(self._string))
        return self._string
    
    def SortWordBySize(self):
        self._string = ' '.join(sorted(list(self._string.split()), key=len))
        return self._string
    
    def SortLexycographic(self):
        self._string = ' '.join(sorted(list(self._string.split())))
        return self._string
        
if __name__ == "__main__":
    stringFmt = StringFormatter("gfjafhur 28 4 8234 rjfwiof r384 uthwe795y 29haweuirh2473 5hwuifb3479 5huiq348h uwar 89q23 4hr89r 2p9rh8934h 20h")
    
    print(stringFmt.DeleteWordsShortestThat(6))
    print(stringFmt.MaskDecimal())
    print(stringFmt.InsertWhiteSpace())
    print(stringFmt.SortWordBySize())
    print(stringFmt.SortLexycographic())
    