#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import struct
import glob

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Application for view IDv3 tag')
    parser.add_argument('-d', '--dump', type=bool, default=False)
    parser.add_argument('-g', '--genre', type=int, default=255)
    parser.add_argument('-i', '--input', type=str)
    args = parser.parse_args()
    
    files = glob.glob(args.input + '/*.mp3')
    
    for file in files:
        with open(file, 'rb+') as f:
            print(file)
            f.seek(-128, 2)
            tag = f.read()
            
            identifier, name, artist, album, year, comment, zero_byte, track, genre = struct.unpack('=3s30s30s30s1i28s1B1B1B', tag)
            
            if identifier != b'TAG':
                continue
            
            if args.dump:
                print(tag, '\n')
            
            if genre == 255:
                if args.genre != 255:
                    genre = args.genre
                    
                    f.seek(-128, 2)
                    f.write(struct.pack('=3s30s30s30s1i28s1B1B1B', identifier, name, artist, album, year, comment, zero_byte, track, genre))
                            
            print("[%s] - [%s] - [%s]" % (name.decode('utf-8'), artist.decode('utf-8'), album.decode('utf-8')))
            